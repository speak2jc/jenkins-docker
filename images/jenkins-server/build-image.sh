#!/bin/bash -eux

if [ $# == 1 ]; then
    VERSION=$1
else
    VERSION=latest
fi

docker build -t speak2jc/jenkins-server:$VERSION .