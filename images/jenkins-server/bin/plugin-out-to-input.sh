#!/bin/bash -eux

# This is a utility to generate a list of plugins in a format that can be processed by Jenkins install-plugins.sh
# This assumes we start with an extract taken from a working Jenkins
# and output in a format that can be imported

# Convert the output of the list-plugins to the id:version format required to import the plugins when building

SCRIPTPATH=$( cd $(dirname $0) ; pwd -P )


$SCRIPTPATH/jcli list-plugins | sed -e "s/^\([a-z-]*\) \([a-zA-Z \_#:().\-]*\) \([0-9.]*\).*$/\1:\3/"


