#!/bin/bash -x

mkdir -p "$PWD"/jenkins_home
chmod 777 "$PWD"/jenkins_home

mkdir -p "$PWD"/.m2
chmod 777 "$PWD"/.m2

# This is for the mac to make it run on port 2376
# Also required on Linux (e.g. AWS EC2)
docker run --name socat -d -v /var/run/docker.sock:/var/run/docker.sock -p 2376:2375 speak2jc/socat TCP4-LISTEN:2375,fork,reuseaddr UNIX-CONNECT:/var/run/docker.sock

docker run -p 8080:8080 -p 50000:50000 -v "$PWD"/jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -v "$PWD"/.m2/:/root/.m2/ -d --name jenkins-server speak2jc/jenkins-server
