import hudson.model.*;
import jenkins.model.*;


Thread.start {
      sleep 10000
      println "--> setting agent port for jnlp"
      def env = System.getenv()
      String port = env['JENKINS_SLAVE_AGENT_PORT']
      int portToUse = port != null ? port : 0
      println "--> Using agent port " + portToUse
      Jenkins.instance.setSlaveAgentPort(portToUse.toInteger())
      println "--> Agent port set to " + portToUse + " for jnlp... done"
      Set<String> protocols = [ "JNLP-connect", "JNLP2-connect", "JNLP4-connect"]
      println "Using protocols " + protocols
      Jenkins.instance.setAgentProtocols(protocols)
}
