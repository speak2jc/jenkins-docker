Preface
Note that I use project and job interchangeably in Jenkins (the Jenkins doco seems to do likewise so I'm not
sure which is correct.

There are clever ways of creating Jenkins jobs programmatically but I think the easiest is by crafting a config.xml
file (which can be generated the first time by creating a job manually and exporting it or by copying an existing
job).

Background

Empty Jenkins Approach
The pattern followed is that bootstrap starts from scratch on EC2 and sets up a running Jenkins
instance with all plugins installed and with Docker plugin configured - one default slave will also
have been added and maybe a default project for demo purposes.

Seed Job Approach
A slightly different pattern would, instead of setting up default slave and project, set up all Jenkins projects that
might be required. Given that nothing in the bootstrap has any knowledge about what projects are required, one
 way of achieving this is to set up a "seed" project during bootstrap which, when run, calls out to a specific git repo for
 this information - the question is: where would this repo live and who would maintain it? Another method is for
 the seed project to have a list of registered projects (by git repo) which it would then visit, looking for the
 Jenkins project data. This has the advantage of pushing the reponsibility for creating the project back to the
 owner of the service. However the seed project would need to download each whole project simply to pick up a pair of
 files (sometimes only one).

What's in this folder?
 Until I've been convinced about the above seed project approach, I'm keeping with Plan A - bootstrapping an empty
 project list and adding projects as needed. the idea would be for the service owner/developer to create the
 config.xml and, if necessary, the create-slave groovy script and store it as part of the codebase. It would also
 be their responsibility to have the scripts run.

 The Maven commands in this folder provide an example, based on the sample pom.xml, of how to create project, template
 or both. Essentially they are wrappers for command line instructions (also in these files).
